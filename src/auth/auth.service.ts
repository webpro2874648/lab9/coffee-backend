import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService,
        private jwtService: JwtService,
    ) { }

    async signIn(email: string, pass: string): Promise<any> {
        try {
            const user = await this.usersService.findOneByEmail(email);

            if (user?.password !== pass) {
                //check password
                throw new UnauthorizedException();
            }
            const payload = { id: user.id, email: user.email };
            const { password, ...result } = user
            return {
                users: result,
                access_token: await this.jwtService.signAsync(payload), //jwt create encrypt
            };
        } catch (e) {
            throw new UnauthorizedException();
        }

    }
}
